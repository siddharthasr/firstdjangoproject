from django.shortcuts import render
from django.http import HttpResponse

def defaultViewFunction(request) :
    return HttpResponse("Siddhartha says : You should have given a proper URL. Empty URL means default view.") ;
