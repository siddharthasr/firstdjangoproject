from django.conf.urls import url
from . import views_finaltest1
from . import views_finaltest2

urlpatterns = [url(r'^$', views_finaltest1.finalTestMessage, name='finalTestMessage'),
        url(r'^test2$', views_finaltest2.finalTestMessage2, name='finalTestMessage2'), ]
